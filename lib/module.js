import path from 'path';

module.exports = function nuxtSketch (_moduleOptions) {
  this.extendRoutes((routes) => {
    routes.push({
      name: 'sketch-bulmacss-elements',
      path: '/sketch/bulmacss/elements',
      component: path.resolve(__dirname, 'pages/elements.vue')
    });
    routes.push({
      name: 'sketch-bulmacss-components',
      path: '/sketch/bulmacss/components',
      component: path.resolve(__dirname, 'pages/components.vue')
    });
  });

  // Add a layout
  this.addLayout(
    path.resolve(__dirname, 'layouts/sketch.vue'),
    'sketch-layout'
  );
    
}

module.exports.meta = require('../package.json');
